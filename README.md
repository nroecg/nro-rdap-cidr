This git repostiory describes the NRO's RDAP extension for expressing CIDR ranges
for IP networks in RDAP.

The source of the specification is in [nro-rdap-cidr.xml](nro-rdap-cidr.xml) in
xml2rfc format. That is used to build the [html](nro-rdap-cidr.html) and [text](nro-rdap-cidr.txt)
versions.

A [markdown](nro-rdap-cidr.md) is created using the Ruby gem `reverse_markdown`.
