| | A. Newton |
| | ARIN |
| | July 23, 2018 |

CIDR Expressions in RDAP  
nro-rdap-cidr

# Abstract

This document describes an extension to RDAP to express Classless Inter-Domain Routing Notation in RDAP.

* * *

# Table of Contents

- 1. Introduction
- 2. RDAP Extension Identifier
- 3. CIDR Notations

  - 3.1. CIDRs
  - 3.2. Example Result
- 4. IANA Considerations
- 5. Normative References
- Author's Address

# 1. Introduction

The Registration Data Access Protocol (RDAP) models IP networks in registries. These IP network objects denote the networks with the start IP address and end IP address of the networks. RDAP models IP networks in this way because some network registrations cannot be expressed with a single Classless Inter-Domain Routing (CIDR) notation (these types of networks are sometimes referred to as "off-bit" registrations).

While "off-bit" networks cannot be expressed with a single CIDR notation, they can be expressed with a series of CIDR notations. This document specifies a simple extension to RDAP for this purpose.

# 2. RDAP Extension Identifier

This document uses the RDAP extension identifier "cidr0" in accordance with Section 2.1 of RFC 7483. The registration for the identifier can be found in Section 4 in accordance with Section 8.1 of RFC 7480.

# 3. CIDR Notations

This extension defines one new JSON data structures for RDAP: an array of objects, where each object represents a single CIDR notation.

The formal syntax specified in JSON Content Rules (JCR).

    $v4cidr = { "v4prefix" : ipv4, "length" : 0..32 } $v6cidr = { "v6prefix" : ipv6, "length" : 0..128 } $cidrs = "cidr0\_cidrs" : [$v4cidr + | $v6cidr +] 

Figure 1: Formal Syntax in JCR

# 3.1. CIDRs

Each IP network defined in Section 5.4 of RFC 7483 may contain an array named "cidr0\_cidrs". This array contains either objects representing IPv4 CIDR notations or IPv6 CIDR notations. Each CIDR notation is presented as an IP address and a length. This array must not be empty for servers supporting this extension.

The formal syntax definition is $cidrs in Figure 1.

# 3.2. Example Result

The following is an elided example of an IP network with CIDR notation.

    { "rdapConformance" : ["rdap\_level\_0", "cidr0"], "objectClassName" : "ip network", "handle" : "XXXX-2", "startAddress" : "2001:db8::", "endAddress" : "2001:db8:0:ffff:ffff:ffff:ffff:ffff", "ipVersion" : "v6", "cidr0\_cidrs" : [{ "v6prefix" : "2001:db8::", "length" : 32 }], ... } 

Figure 2: Elided IP Network Example

# 4. IANA Considerations

The information below will be registered with the IANA according to the section 8.1 of RFC 7480.

Extension identifier: cidr0

Registry operator: Various RIRs

Published specification: this document

Person & email address to contact for further information: andy@arin.net

Intended usage: common.

# 5. Normative References

| **[1]** | Fuller, V. and T. Li, "[Classless Inter-domain Routing (CIDR): The Internet Address Assignment and Aggregation Plan](https://tools.ietf.org/html/rfc4632)", BCP 122, RFC 4632, DOI 10.17487/RFC4632, August 2006. |
| **[2]** | Bray, T., "[The JavaScript Object Notation (JSON) Data Interchange Format](https://tools.ietf.org/html/rfc7159)", RFC 7159, DOI 10.17487/RFC7159, March 2014. |
| **[3]** | Newton, A., Ellacott, B. and N. Kong, "[HTTP Usage in the Registration Data Access Protocol (RDAP)](https://tools.ietf.org/html/rfc7480)", RFC 7480, DOI 10.17487/RFC7480, March 2015. |
| **[4]** | Newton, A. and S. Hollenbeck, "[Registration Data Access Protocol (RDAP) Query Format](https://tools.ietf.org/html/rfc7482)", RFC 7482, DOI 10.17487/RFC7482, March 2015. |
| **[5]** | Newton, A. and S. Hollenbeck, "[JSON Responses for the Registration Data Access Protocol (RDAP)](https://tools.ietf.org/html/rfc7483)", RFC 7483, DOI 10.17487/RFC7483, March 2015. |
| **[6]** | Newton, A. and P. Cordell, "[A Language for Rules Describing JSON Content](https://tools.ietf.org/html/draft-newton-json-content-rules-09)", Internet-Draft draft-newton-json-content-rules-09, September 2017. |
| **[7]** | Newton, A., "[A Description of RDAP JSON Messages Using JSON Content Rules](https://tools.ietf.org/html/draft-newton-rdap-jcr-06)", Internet-Draft draft-newton-rdap-jcr-06, January 2018. |

# Author's Address

<address class="vcard">
	<span class="vcardline">
	  <span class="fn">Andrew Lee Newton</span> 
	  <span class="n hidden">
		<span class="family-name">Newton</span>
	  </span>
	</span>
	<span class="org vcardline">American Registry for Internet Numbers</span>
	<span class="adr">
	  <span class="vcardline">PO Box 232290</span>

	  <span class="vcardline">
		<span class="locality">Centerville</span>,  
		<span class="region">VA</span> 
		<span class="code">20120</span>
	  </span>
	  <span class="country-name vcardline">US</span>
	</span>
	<span class="vcardline">EMail: <a href="mailto:andy@arin.net">andy@arin.net</a></span>

<span class="vcardline">URI: <a href="http://www.arin.net">http://www.arin.net</a></span>

  </address>
