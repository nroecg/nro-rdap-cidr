<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE rfc SYSTEM "http://xml.resource.org/authoring/rfc2629.dtd"
[
<!ENTITY RFC4632 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.4632.xml'>
<!ENTITY RFC7159 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.7159.xml'>
<!ENTITY RFC7480 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.7480.xml'>
<!ENTITY RFC7482 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.7482.xml'>
<!ENTITY RFC7483 PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml/reference.RFC.7483.xml'>
<!ENTITY I-D.newton-json-content-rules PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml3/reference.I-D.newton-json-content-rules.xml'>
<!ENTITY I-D.newton-rdap-jcr PUBLIC ''
   'http://xml.resource.org/public/rfc/bibxml3/reference.I-D.newton-rdap-jcr.xml'>
]>
<?rfc toc="yes"?>
<?rfc symrefs="no"?>
<?rfc private="yes"?>
<rfc category="info" docName="nro-rdap-cidr">
    <front>
        <title abbrev="RDAP CIDR">CIDR Expressions in RDAP</title>
        <author fullname="Andrew Lee Newton" initials="A.L." surname="Newton">
            <organization abbrev="ARIN">American Registry for Internet Numbers</organization>
            <address>
                <postal>
                    <street>PO Box 232290</street>
                    <city>Centerville</city>
                    <region>VA</region>
                    <country>US</country>
                    <code>20120</code>
                </postal>
                <email>andy@arin.net</email>
                <uri>http://www.arin.net</uri>
            </address>
        </author>
        <date/>
        <abstract>
            <t>
		This document describes an extension to RDAP to express Classless Inter-Domain Routing Notation in RDAP.
            </t>
        </abstract>
    </front>
    <middle>
        <section title="Introduction">
            <t>
                The Registration Data Access Protocol (<xref target="RFC7483">RDAP</xref>) models IP networks in
                registries. These IP network objects denote the networks with the start IP address and end IP address
                of the networks. <xref target="RFC7483">RDAP</xref> models IP networks in this way because some network
                registrations cannot be expressed with a single Classless Inter-Domain Routing (<xref target="RFC4632">CIDR</xref>)
                notation (these types of networks are sometimes referred to as "off-bit" registrations).
            </t>
            <t>
                While "off-bit" networks cannot be expressed with a single CIDR notation, they can be expressed with
                a series of CIDR notations. This document specifies a simple extension to RDAP for this purpose.
            </t>
        </section>
        <section title="RDAP Extension Identifier">
            <t>
                This document uses the RDAP extension identifier "cidr0" in accordance with
                Section 2.1 of <xref target="RFC7483">RFC 7483</xref>. The registration for the
                identifier can be found in <xref target="iana-considerations"></xref> in accordance with Section 8.1 of
                <xref target="RFC7480">RFC 7480</xref>.
            </t>
        </section>
        <section title="CIDR Notations" anchor="cidr-notations">
            <t>
                This extension defines one new JSON data structures for RDAP: an array of objects, where each object
                represents a single CIDR notation.
            </t>
            <figure align="center" title="Formal Syntax in JCR" anchor="jcr">
                <preamble>The formal syntax specified in <xref target="I-D.newton-json-content-rules">JSON Content Rules (JCR)</xref>.</preamble>
                <artwork xml:space="preserve" align="center">
$v4cidr = {
    "v4prefix" : ipv4,
    "length"   : 0..32
}

$v6cidr = {
    "v6prefix" : ipv6,
    "length"   : 0..128
}

$cidrs = "cidr0_cidrs" : [
    $v4cidr + | $v6cidr +
]
                </artwork>
            </figure>
            <section title="CIDRs" anchor="cidrs">
                <t>
                    Each IP network defined in Section 5.4 of <xref target="RFC7483">RFC 7483</xref> may contain an array named
                    "cidr0_cidrs". This array contains either objects representing IPv4 CIDR notations or IPv6 CIDR
                    notations. Each CIDR notation is presented as an IP address and a length.
                    This array must not be empty for servers supporting this extension.
                </t>
                <t>
                    The formal syntax definition is $cidrs in <xref target="jcr"></xref>.
                </t>
            </section>
            <section title="Example Result" anchor="example-result">
                <figure align="center" title="Elided IP Network Example" anchor="elided-net-example.json">
                    <preamble>The following is an elided example of an IP network with CIDR notation.</preamble>
                    <artwork xml:space="preserve" align="center">
{
  "rdapConformance" : [ "rdap_level_0", "cidr0" ],
  "objectClassName" : "ip network",
  "handle" : "XXXX-2",
  "startAddress" : "2001:db8::",
  "endAddress" : "2001:db8:0:ffff:ffff:ffff:ffff:ffff",
  "ipVersion" : "v6",
  "cidr0_cidrs" : [
    {
      "v6prefix" : "2001:db8::",
      "length"   : 32
    }
  ],
  ...
}
                    </artwork>
                </figure>
            </section>
        </section>
        <section title="IANA Considerations" anchor="iana-considerations">
            <t>
              The information below will be registered
              with the IANA according to the section 8.1 of <xref target="RFC7480">RFC 7480</xref>.
            </t>
            <t>
                Extension identifier: cidr0
            </t>
            <t>
                Registry operator: Various RIRs
            </t>
            <t>
                Published specification: this document
            </t>
            <t>
                Person &amp; email address to contact for further information: andy@arin.net
            </t>
            <t>
                Intended usage: common.
            </t>
        </section>
    </middle>
    <back>
        <references title="Normative References">

            &RFC4632;
            &RFC7159;
            &RFC7480;
            &RFC7482;
            &RFC7483;
            &I-D.newton-json-content-rules;
            &I-D.newton-rdap-jcr;

        </references>
    </back>
</rfc>
